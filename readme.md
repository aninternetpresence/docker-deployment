# AIP - Docker

This is the Docker Compose setup for An Internet Presence. It consists of:

* **blog** - The main WordPress blog at aninternetpresence.net
* **demos** - The demos.aninternetpresence.net site
* **would-you-rather** - The wyr.aninternetpresence.net site with the final React project for Udemy.

The actual code that is deployed in the docker environments exists in other repos found in this project.

The services defined in the `docker-compose.yml` are as as follows:

* `blog` - A `wordpress` container for the AIP blog
* `blog_db` - A `mariadb` container for the AIP blog
* `demos_php` - A `php:fpm` container for AIP Demos
* `demos_apache` - An `httpd` container for AIP Demos
* `demos_db` - A `mariadb` container for AIP Demos
* `would_you_rather` - A `node` container for would-you-rather

## Deployment

Please read this whole document to familiarize yourself with exactly how things are setup. You will generally be following these steps:

1. Clone this repo and all submodules: `git clone --recurse-submodules git@gitlab.com:aninternetpresence/docker-deployment.git`
2. Copy one of the `.env.*.example` files to `.env` and set it accordingly.
3. Copy one of the `docker-compose.*.yml.example` to `docker-compose.*.yml`. For example, `docker-compose.local.yml.example` can be copied to `docker-compose.local.yml` and edited for local/dev deployment. There is also a prod deployment `.yml`.
Please use that one for production deployment.
4. Check these config files to make sure they have what you really want.
4. Create or obtain an SSL certificate. Put the key and cert into a file called `ssl-cert-snakeoil.pem` and copy it to:
   * `blog/wordpress/ssl/certs`
   * `demos/apache/ssl/certs`
5. Run `docker-compose up -d` to build and run everything.

If you make changes to a container's image, rebuild and run it with:

    docker-compose up --build --no-deps -d [container]

## Making a self-signed cert

Make sure you are using the latest version of OpenSSL or LibreSSL, then run the following command:

```shell
openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 30650 \
            -nodes \
            -out aip.crt \
            -keyout aip.key \
            -subj "/C=US/ST=Arizona/L=Phoenix/O=An Internet Presence/OU=Owner/CN=aninternetpresence.local" \
            -addext "subjectAltName=DNS:*.aninternetpresence.local,DNS:aninternetpresence.local"
```

> [!NOTE]  
> If using PowerShell, replace the backslashes with backticks

Combine these files with a command like:

```shell
cat aip.key aip.crt > ssl-cert-snakeoil.pem
```

or a command like the following for Powershell

```Powershell
Get-Content .\aip.crt,.\aip.key | Out-File .\ssl-cert-snakeoil.pem
```

Now you can copy `ssl-cert-snakeoil.pem` to the necessary folders.

## Environment settings

There are a few example `.env` files to get started. Copy one of them to a `.env` and edit this file as appropriate for the environment, using the example `yml` files as guides, to know what env settings these files expect. You will need this `.env` file for a successful deployment. It consists of values like the following:

* `COMPOSE_PROJECT_NAME` - A special value to Docker, used to set a prefix for the container and volume names.

* `COMPOSE_FILE` - Set this to a colon-delimited list of `yml` files. E.g. `docker-compose.yml:docker-compose.prod.yml`. Use this variable in replacement of the `-f` flag.

* `IMAGE_NS` - The prefix for the image name that is created for the service.

* `BLOG_DB_PORT` - The host port that will forward to the same port in the `blog_db` container.

* `BLOG_PORT_80` - The host port that will forward HTTP requests to the `blog` container.

* `BLOG_PORT_443` - The host port that will forward SSL requests to the `blog` container.

* `BLOG_WP_VOLUME` - In a local environment, set this to a local folder path to make a bind-mounted volume for the WordPress site. In a prod-like environment, set this to the name of the docker volume that the blog site will be deployed to in the container.

* `BLOG_DASHBOARD_UPGRADES` - Set this in your `.env` to any value if you want users to be able to download upgrades from the admin dashboard.

* `DEMOS_APACHE_PORT_80` - The host port that will forward HTTP requests to the `demos_apache` container.

* `DEMOS_APACHE_PORT_443` - The host port that will forward SSL requests to the `demos_apache` container.

* `DEMOS_DB_PORT` - The host port that will forward to the same port in the `demos_db` container.

* `DEMOS_PHP_PORT` - The port that the `demos_php` FPM process will listen on in that container.

* `DEMOS_PHP_WORKDIR` - The directory for the Demos site files in the `demos_php` container.

* `DEMOS_VOLUME` - In a local environment, set this to a local folder path to make a bind-mounted volume for the AIP Demos site. In a prod-like environment, set this to the name of the docker volume that the Demos site will be deployed to in the container.

* `BLOG_SERVER_NAME` - The server name of the AIP blog site.

* `DEMOS_SERVER_NAME` - The server name of the AIP Demos site.

* `WYR_PORT` - The host port that will forward HTTP requests to the `would_you_rather` container.

* `WYR_VOLUME` - In a local environment, set this to the absolute path of the would-you-rather site. In a prod-like environment, set this to the name of the volume that the would-you-rather site will be deployed to in the container.

The following are typically only set in a production environment:

* `MYSQL_ROOT_PASSWORD_DEMOS` - The root password of the DB on `demos_db`.

* `MYSQL_PASSWORD_DEMOS` - The main user password of the DB on `demos_db`.

* `MYSQL_ROOT_PASSWORD_BLOG` - The root password of the DB on `blog_db`.

* `MYSQL_PASSWORD_BLOG` - The main user password of the DB on `blog_db`.

* `GIT_REMOTE_DEMOS` - The git URL of the AIP Demos site for production deployment.

* `GIT_REMOTE_BLOG` - The git URL of the AIP WordPress uploads folder for production deployment.

* `GIT_REMOTE_WYR` - The git URL of the would-you-rather site for production deployment.

* `DEV_MODE` - Set this to a value in order to enable development features like Xdebug and dependencies for Laravel Dusk.

You can alternatively specify a `.env.local` and/or `.env.prod` file. In the corresponding `docker-compose.yml` add a `env_file:` setting for the service in order to use it. Otherwise, `.env` will be used.

##  Local Development

### httpd for Windows

On a Windows host, [httpd for Windows from Apache Lounge](https://www.apachelounge.com/download/) is recommended.

On the download page, look for the link to the Visual C++ Redistributable package. Make sure you install this first. Then download the ZIP for the latest Apache 2.4. Now, follow these instructions:

* Please copy the `Apache24` folder from the ZIP file to `C:\Apache24` on your system.
  * Be sure to add `C:\Apache24\bin` to your `PATH`. This will let you run `httpd` and `openssl`.
* Take a look at the ReadMe.txt from the ZIP file
* Follow the instructions to add httpd as a service. 
  * Open a terminal as an administrator
  * Simply run the command `httpd -k install`
  * Run `services.msc` and confirm that Apache2.4 is listed as a Running service with Automatic Startup Type.
* In File Explorer, go to `shell:Startup` to open your Startup folder. This is where you can create shortcuts to programs so they startup on system start.
* Create a shortcut for `C:\Apache24\bin\ApacheMonitor.exe` in your Startup folder
  * You can use this program to manage the httpd service, but there are many times where command line commands will provide better feedback

#### Recommended Host Web Server Configuration

When using httpd on the host in a local environment, the following conf is recommended:

* Edit `C:\Apache24\conf\httpd.conf` and enable the following modules:
  * proxy_module
  * proxy_http_module
  * ssl_module
* At the bottom of `httpd.conf`, add the line `IncludeOptional conf/sites/*.conf`
* Create `C:\Apache24\conf\sites\aip.conf` with the following file contents:

```apache
<VirtualHost *:80>
    # Redirect requests for port 80 to port 443
    ServerName aninternetpresence.local
    Redirect permanent / https://aninternetpresence.local:443/
</VirtualHost>

<VirtualHost *:443> # Handle requests for port 443
    ServerName aninternetpresence.local
    
    ErrorLog "C:\Apache24\logs\aninternetpresence.local-error.log"
    TransferLog "C:\Apache24\logs\aninternetpresence.local-access.log"
    
    SSLEngine On
    SSLCertificateFile C:\Apache24\ssl\aip.pem
    
    SSLProxyEngine On
    ProxyRequests Off
    ProxyVia Block
    ProxyPreserveHost On
     
    <Proxy *>
         Require all granted
    </Proxy>
    
    # Send everything to port 8004, where the cmd_httpd container is listening and will forward internally to 443
    ProxyPass / https://127.0.0.1:8005/
    ProxyPassReverse / https://127.0.0.1:8005/
</VirtualHost>

<VirtualHost *:80>
    # Redirect requests for port 80 to port 443
    ServerName demos.aninternetpresence.local
    Redirect permanent / https://demos.aninternetpresence.local:443/
</VirtualHost>

<VirtualHost *:443>
    # Redirect requests for port 80 to port 443
    ServerName demos.aninternetpresence.local

    ErrorLog "C:\Apache24\logs\demos.aninternetpresence.local-error.log"
    TransferLog "C:\Apache24\logs\demos.aninternetpresence.local-access.log"

    SSLCertificateFile C:\Apache24\ssl\aip.pem
    
    SSLEngine on
    SSLProxyEngine On
    ProxyRequests Off
    ProxyVia Block
    ProxyPreserveHost On

    <Proxy *>
         Require all granted
    </Proxy>

    ProxyPass / https://127.0.0.1:8002/
    ProxyPassReverse / https://127.0.0.1:8002/
</VirtualHost>
```

You may use any web server on your host to act as a reverse proxy. The above settings are a guide for using httpd on your host for this purpose. Be sure to replace the file paths, server names, and port numbers with what you are using in your environment.

With httpd, you can create local environment variables for the conf files. A good way to do so is to make a conf called `000-vars.conf` and set its contents to `Define SERVER_NAME aninternetpresence.local` so you can refer to this value with `${SERVER_NAME}` in the conf files. You can setup any other httpd conf env vars like this.

## WordPress setup

Any INI files that should be added to PHP's config for the main WordPress site should be copied to `blog/wordpress/ini`.

My [wpgithooks](https://github.com/enderandpeter/wpgithooks) scripts are used to setup and manage the WordPress site. Files that it expects can be
copied to `blog/wordpress/project/.setup` you will see initial example files to start with.

Be sure to add a `blog/wordpress/project/.git/config` file that will likely look like the following:

```
[wp-site]
	local = aninternetpresence.local
    local2 = aninternetpresence2.local
	live = aninternetpresence.net
	deploy = local 
```

Set the `deploy` key to the environment that is being deployed.

Use `blog/wordpress/project/.setup/wp-addons.php` to setup the themes and plugins that should be installed or deleted. Also, you can add the database
credentials that will be used when setting up the site the first time which will be set in the new wp-config.php file.